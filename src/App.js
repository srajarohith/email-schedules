import "./App.css";
import React from "react";
import { Layout, Menu, theme } from "antd";
import Dashboard from "./screens/dashboard";

const { Header, Content, Sider } = Layout;

const App = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        style={{ background: "#391e5a" }}
        width={72}
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      ></Sider>
      <Layout>
        <Header
          style={{
            backgroundColor: "#D8D2DE",
            height: 70,
            padding: 0,
            background: colorBgContainer,
          }}
        />
        <Content
          style={{
            margin: "24px 16px 0",
          }}
        >
          <div
            style={{
              padding: 24,
              minHeight: 660,
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            <Dashboard />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};
export default App;
