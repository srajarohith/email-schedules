import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
  name: "EmailData",
  initialState: {
    value: 0,
    scheduleList: [],
  },
  reducers: {
    addScheduleEmail: (state, action) => {
      state?.scheduleList.push(action?.payload);
    },
    deleteScheduleEmail: (state, action) => {
      for (let i = 0; i < state?.scheduleList.length; i++) {
        if (state?.scheduleList[i].key === action?.payload) {
          state?.scheduleList?.splice(i, 1);
          return;
        }
      }
    },
    editScheduleEmail: (state, action) => {
      state.scheduleList = state.scheduleList.map((item) =>
        item.key === action?.payload?.key ? action?.payload : item
      );
    },
  },
});

export const { addScheduleEmail, deleteScheduleEmail, editScheduleEmail } =
  counterSlice.actions;

export default counterSlice.reducer;
