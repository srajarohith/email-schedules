import { configureStore } from "@reduxjs/toolkit";
import EmailDataReducer from "./reducers/EmailDataReducer";

export default configureStore({
  reducer: {
    emailData: EmailDataReducer,
  },
});
