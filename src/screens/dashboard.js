import React, { useEffect, useState } from "react";
import Search from "antd/es/input/Search";
import { Space, Table } from "antd";
import { Button, Col, Popover, Row, Form } from "antd";
import {
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import ScheduleForm from "../components/scheduleForm";
import { useDispatch, useSelector } from "react-redux";
import { deleteScheduleEmail } from "../store/reducers/EmailDataReducer";

const Dashboard = () => {
  const store = useSelector((state) => state?.emailData);

  const [filterInput, setFilterInput] = useState("");

  const onSearch = (value) => setFilterInput(value);

  const dispatch = useDispatch();

  const deleteSchedule = (value) => {
    dispatch(deleteScheduleEmail(value));
  };

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
      width: "50%",
    },
    {
      title: "Subject",
      dataIndex: "subject",
      key: "subject",
    },
    {
      title: "Schedule",
      key: "schedule",
      render: (data) => (
        <Space size="middle">
          {data?.frequency == "Daily" ? (
            <p>{`${data?.frequency} at ${data?.time}`}</p>
          ) : (
            <p>{`${data?.frequency} ${data?.repeat} at ${data?.time}`}</p>
          )}
        </Space>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (data) => (
        <Space size="middle">
          <a>
            <Popover
              placement="leftTop"
              title={"Edit Schedule"}
              content={<ScheduleForm rowData={data} />}
              arrow={false}
            >
              <EditOutlined />
            </Popover>
          </a>
          <a onClick={() => deleteSchedule(data?.key)}>
            <DeleteOutlined />
          </a>
        </Space>
      ),
    },
  ];

  const filterData = () => {
    if (filterInput === "") return store?.scheduleList;

    if (isNaN(filterInput)) {
      return store?.scheduleList?.filter(({ title }) =>
        title.toLowerCase().includes(filterInput.toLowerCase())
      );
    }
  };

  return (
    <Col>
      <Row style={{ justifyContent: "space-between" }}>
        <Search
          placeholder="Search Title"
          allowClear
          onSearch={onSearch}
          style={{
            width: 304,
          }}
        />
        <Popover
          placement="leftTop"
          title={"Add Schedule"}
          content={<ScheduleForm />}
          arrow={false}
        >
          <Button
            style={{ background: "#391E5A" }}
            type="primary"
            icon={<PlusCircleOutlined />}
            size={"middle"}
          >
            Add
          </Button>
        </Popover>
      </Row>
      <div style={{ paddingTop: 20 }}>
        <Table
          columns={columns}
          size="middle"
          dataSource={filterData()}
          pagination={false}
          scroll={{
            y: "60vh",
          }}
        />
      </div>
    </Col>
  );
};

export default Dashboard;
