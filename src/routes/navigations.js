import { Route, Routes } from "react-router-dom";
import App from "../App";

export default function Navigations() {
  return (
    <>
      <Routes>
        <Route path="/" element={<App />}></Route>
        <Route
          path="*"
          element={
            <main
              style={{
                padding: "1rem",
              }}
            >
              <p
                style={{
                  display: "flex",
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                There's nothing here!
              </p>
            </main>
          }
        />
      </Routes>
    </>
  );
}
