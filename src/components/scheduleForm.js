import { Button, Form, Input, Radio, Row, Select } from "antd";
import TextArea from "antd/es/input/TextArea";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addScheduleEmail,
  editScheduleEmail,
} from "../store/reducers/EmailDataReducer";

const ScheduleForm = (data) => {
  const [form] = Form.useForm();

  const dispatch = useDispatch();

  const [frequency, setFrequency] = useState(" ");

  useEffect(() => {
    if (data?.rowData) {
      form.setFieldsValue({
        key: data?.rowData?.key,
        title: data?.rowData?.title,
        description: data?.rowData?.description,
        subject: data?.rowData?.subject,
        frequency: data?.rowData?.frequency,
        repeat: data?.rowData?.repeat,
        time: data?.rowData?.time,
      });
    }
  }, [data]);

  const repeatDays = [
    {
      label: "S",
      value: "Sunday",
    },
    {
      label: "M",
      value: "Monday",
    },
    {
      label: "T",
      value: "Tuesday",
    },
    {
      label: "W",
      value: "Wednesday",
    },
    {
      label: "T",
      value: "Thursday",
    },
    {
      label: "F",
      value: "Friday",
    },
    {
      label: "S",
      value: "Saturday",
    },
  ];

  const buttonItemLayout = {
    wrapperCol: {
      span: 24,
      offset: 4,
    },
  };

  const store = useSelector((state) => state?.emailData);

  const submitForm = (values) => {
    const payload = {
      key: data?.rowData ? data?.rowData?.key : store?.scheduleList?.length + 1,
      title: values?.title,
      description: values?.description,
      subject: values?.subject,
      frequency: values?.frequency,
      repeat: values?.repeat,
      time: values.time,
    };
    if (data?.rowData) {
      dispatch(editScheduleEmail(payload));
    } else {
      dispatch(addScheduleEmail(payload));
    }
    form.resetFields();
  };

  const hours = Array.from({ length: 12 }, (_, index) => index + 1);

  return (
    <Form
      layout={"horizontal"}
      form={form}
      initialValues={{
        remember: true,
      }}
      onFinish={submitForm}
      style={{
        maxWidth: 600,
      }}
    >
      <Form.Item
        label="Title"
        name="title"
        rules={[
          {
            required: true,
            pattern: new RegExp(/^[a-zA-Z.0-9 ]{0,20}$/),
            message: "Enter a valid Title",
          },
        ]}
      >
        <Input placeholder="Enter Title" />
      </Form.Item>
      <Form.Item
        label="Description"
        name="description"
        rules={[
          {
            required: true,
            pattern: new RegExp(/^[a-zA-Z.0-9 ]{0,100}$/),
            message: "Enter a valid Description",
          },
        ]}
      >
        <TextArea rows={3} />
      </Form.Item>
      <Form.Item
        label="Subject"
        name="subject"
        rules={[
          {
            required: true,
            pattern: new RegExp(/^[a-zA-Z.0-9 ]{0,20}$/),
            message: "Enter a valid Subject",
          },
        ]}
      >
        <Input placeholder="Enter Subject" />
      </Form.Item>
      <Form.Item
        label="Frequency"
        name="frequency"
        rules={[
          {
            required: true,
            message: "Frequency value is Required",
          },
        ]}
      >
        <Select onChange={(value) => setFrequency(value)}>
          <Select.Option value="Daily">Daily</Select.Option>
          <Select.Option value="Weekly">Weekly</Select.Option>
          <Select.Option value="Monthly">Monthly</Select.Option>
        </Select>
      </Form.Item>
      {frequency == "Weekly" || data?.rowData?.frequency == "Weekly" ? (
        <Form.Item
          label="Repeat"
          name="repeat"
          rules={[
            {
              required: true,
              message: "Repeat value is Required",
            },
          ]}
        >
          <Radio.Group>
            {repeatDays.map((day) => (
              <Radio.Button key={day.value} value={day.value}>
                {day.label}
              </Radio.Button>
            ))}
          </Radio.Group>
        </Form.Item>
      ) : frequency == "Monthly" || data?.rowData?.frequency == "Monthly" ? (
        <Form.Item
          label="Repeat"
          name="repeat"
          rules={[
            {
              required: true,
              message: "Repeat value is Required",
            },
          ]}
        >
          <Select>
            <Select.Option key={1} value={"First Monday"}>
              {"First Monday"}
            </Select.Option>
            <Select.Option key={2} value={"Last Friday"}>
              {"Last Friday"}
            </Select.Option>
          </Select>
        </Form.Item>
      ) : null}

      <Form.Item
        label="Time"
        name="time"
        rules={[
          {
            required: true,
            message: "Time value is Required",
          },
        ]}
      >
        <Select>
          {hours.flatMap((hour) => (
            <Select.Option
              key={`${hour}:00 AM`}
              value={`${hour}:00 AM`}
            >{`${hour}:00 AM`}</Select.Option>
          ))}
          {hours.flatMap((hour) => (
            <Select.Option
              key={`${hour}:00 PM`}
              value={`${hour}:00 PM`}
            >{`${hour}:00 PM`}</Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item {...buttonItemLayout}>
        <Row style={{ justifyContent: "flex-end" }}>
          <Button style={{ marginRight: 8 }}>Cancel</Button>
          <Button
            style={{ background: "#391E5A" }}
            type="primary"
            htmlType="submit"
          >
            Done
          </Button>
        </Row>
      </Form.Item>
    </Form>
  );
};

export default ScheduleForm;
